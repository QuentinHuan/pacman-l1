# Pacman L1

5 séances en tout.
4 points par séances


**************
*  seance 1  *
**************

Creation niveau et affichage

- lire le fichier de niveau
- créer le tableau de jeu
- afficher sur plusieurs lignes avec print("")
- afficher avec character UNICODE et couleurs

**************
*  seance 2  *
**************

Amélioration de l'affichage:
 - ajout de nouveaux blocs (rouge, vert, bleu...)
 - Exercices de créations de tableaux avec motifs périodiques avec l'opérateur modulo `%` 

 ```
#  motif à bandes
[[0,1,2,3,0,1,2],
 [0,1,2,3,0,1,2],
 [0,1,2,3,0,1,2],
 [0,1,2,3,0,1,2],
 [0,1,2,3,0,1,2],
 [0,1,2,3,0,1,2],
 [0,1,2,3,0,1,2]]
 ``` 

 ```
# motif diagonal
[[0,1,2,3,0,1,2],
 [1,2,3,0,1,2,3],
 [2,3,0,1,2,3,0],
 [3,0,1,2,3,0,1],
 [0,1,2,3,0,1,2],
 [1,2,3,0,1,2,3],
 [2,3,0,1,2,3,0]]
 ``` 
 - Affichage et animations des motifs avec une boucle infini, découverte du module `time` pour mesurer le temps

**************
*  seance 3  *
**************

Les fantômes: définis par leur position x et y
- ajouter un fantôme (créer les variables de positions, afficher avec une fonction `draw_phantomes(p_x,p_y)`)
- créer le fantôme depuis le fichier `level1.lvl`
- modifier le code pour ajouter et afficher plusieurs fantômes
- bouger les fantômes au hasard (sans qu'ils rentrent dans les murs)
- bouger au hasard seulement quand ils rencontrent un obstacle

**************
*  seance 4  *
**************

Entrées clavier, contrôle de PAC man

- bouger avec le clavier sur les cases adjacentes (sans collisions)
- collisions avec les murs
- ajouter des boules sur toutes les cases vides du niveau
- manger les boules et augmenter le score

**************
*  seance 5  *
**************
Interactions fantômes et pacman
- Perdu si le joueur touche un fantôme
- Active le super mode pour x secondes en mangeant une boule spéciale
- Quand Pacman est en super mode, il peut manger les fantômes
- Le niveau s'anime en super mode (réutiliser les patterns vu en séance 2)

******************
*  seance BONUS  *
******************
Amélioration des fantômes:
- utiliser le module de pathfinding fournis pour améliorer l'IA des fantômes
- En mode normal, les fantômes se dirigent vers pacman par le plus cours chemin
- En super mode, les fantomes s'enfuient 
- {...}

On pourrait faire bien d'autres choses...
Une bonne idée serait de laisser tomber le terminal et d'utiliser le module Pygame pour l'affichage et les entrées claviers.
Comme le code est bien compartimenté, il suffit de reprogrammer le module `graphics.py` et la boucle `# input` du fichier main en utilisant les fonctionnalités de Pygame.
La logique du jeux ne change pas, seul les modules d'affichage et de lecture du claviers doivent être modifiées.
