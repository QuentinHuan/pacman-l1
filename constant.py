W = 10 # width of T screen
H = 20 # height of T screen
B = 4  # border size

tetrimino_spawn_x = 5
tetrimino_spawn_y = 0

game_grid_origin_x = W
game_grid_origin_y = 5
game_grid_debug_origin_x = 4*W
game_grid_debug_origin_y = 5

draw_freq = 0.05
update_freq = 0.25 


class bcolors:
    HEADER = '\033[95m'
    BLUE = '\033[94m'
    CYAN = '\033[96m'
    GREEN = '\033[92m'
    MAGENTA = '\033[93m'
    RED = '\033[91m'
    ENDC = '\033[0m'
    BOLD = '\033[1m'
    GRAY = '\033[38;5;242m'
    YELLOW = '\033[38;5;220m'
    UNDERLINE = '\033[4m'

PHANTOMS_COLORS = [bcolors.BLUE,bcolors.RED,bcolors.GREEN,bcolors.CYAN,bcolors.MAGENTA]
