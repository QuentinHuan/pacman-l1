let SessionLoad = 1
if &cp | set nocp | endif
let s:so_save = &g:so | let s:siso_save = &g:siso | setg so=0 siso=0 | setl so=-1 siso=-1
let v:this_session=expand("<sfile>:p")
silent only
silent tabonly
cd ~/Enseignement/L1/Info5/Pacman/pacman-l1
if expand('%') == '' && !&modified && line('$') <= 1 && getline(1) == ''
  let s:wipebuf = bufnr('%')
endif
set shortmess=aoO
argglobal
%argdel
edit pacman.py
let s:save_splitbelow = &splitbelow
let s:save_splitright = &splitright
set splitbelow splitright
wincmd _ | wincmd |
vsplit
1wincmd h
wincmd _ | wincmd |
split
1wincmd k
wincmd w
wincmd w
wincmd _ | wincmd |
split
1wincmd k
wincmd w
let &splitbelow = s:save_splitbelow
let &splitright = s:save_splitright
wincmd t
let s:save_winminheight = &winminheight
let s:save_winminwidth = &winminwidth
set winminheight=0
set winheight=1
set winminwidth=0
set winwidth=1
exe '1resize ' . ((&lines * 27 + 29) / 59)
exe 'vert 1resize ' . ((&columns * 118 + 118) / 237)
exe '2resize ' . ((&lines * 28 + 29) / 59)
exe 'vert 2resize ' . ((&columns * 118 + 118) / 237)
exe '3resize ' . ((&lines * 27 + 29) / 59)
exe 'vert 3resize ' . ((&columns * 118 + 118) / 237)
exe '4resize ' . ((&lines * 28 + 29) / 59)
exe 'vert 4resize ' . ((&columns * 118 + 118) / 237)
argglobal
balt pacman.py
setlocal fdm=indent
setlocal fde=0
setlocal fmr={{{,}}}
setlocal fdi=#
setlocal fdl=0
setlocal fml=2
setlocal fdn=3
setlocal fen
50
normal! zo
70
normal! zo
73
normal! zo
let s:l = 68 - ((10 * winheight(0) + 13) / 27)
if s:l < 1 | let s:l = 1 | endif
keepjumps exe s:l
normal! zt
keepjumps 68
normal! 0
wincmd w
argglobal
if bufexists("graphics.py") | buffer graphics.py | else | edit graphics.py | endif
balt pacman.py
setlocal fdm=indent
setlocal fde=0
setlocal fmr={{{,}}}
setlocal fdi=#
setlocal fdl=0
setlocal fml=2
setlocal fdn=3
setlocal fen
22
normal! zo
23
normal! zo
24
normal! zo
44
normal! zo
45
normal! zo
51
normal! zo
let s:l = 43 - ((17 * winheight(0) + 14) / 28)
if s:l < 1 | let s:l = 1 | endif
keepjumps exe s:l
normal! zt
keepjumps 43
normal! 046|
wincmd w
argglobal
if bufexists("phantoms.py") | buffer phantoms.py | else | edit phantoms.py | endif
balt ~/Enseignement/L1/Info5/Pacman/pacman.py
setlocal fdm=indent
setlocal fde=0
setlocal fmr={{{,}}}
setlocal fdi=#
setlocal fdl=0
setlocal fml=2
setlocal fdn=3
setlocal fen
14
normal! zo
25
normal! zo
33
normal! zo
34
normal! zo
let s:l = 2 - ((1 * winheight(0) + 13) / 27)
if s:l < 1 | let s:l = 1 | endif
keepjumps exe s:l
normal! zt
keepjumps 2
normal! 0
wincmd w
argglobal
if bufexists("pac.py") | buffer pac.py | else | edit pac.py | endif
balt pacman.py
setlocal fdm=indent
setlocal fde=0
setlocal fmr={{{,}}}
setlocal fdi=#
setlocal fdl=0
setlocal fml=2
setlocal fdn=3
setlocal fen
9
normal! zo
18
normal! zo
24
normal! zo
26
normal! zo
27
normal! zo
let s:l = 4 - ((3 * winheight(0) + 14) / 28)
if s:l < 1 | let s:l = 1 | endif
keepjumps exe s:l
normal! zt
keepjumps 4
normal! 015|
wincmd w
exe '1resize ' . ((&lines * 27 + 29) / 59)
exe 'vert 1resize ' . ((&columns * 118 + 118) / 237)
exe '2resize ' . ((&lines * 28 + 29) / 59)
exe 'vert 2resize ' . ((&columns * 118 + 118) / 237)
exe '3resize ' . ((&lines * 27 + 29) / 59)
exe 'vert 3resize ' . ((&columns * 118 + 118) / 237)
exe '4resize ' . ((&lines * 28 + 29) / 59)
exe 'vert 4resize ' . ((&columns * 118 + 118) / 237)
tabnext 1
badd +1 pacman.py
badd +4 pac.py
badd +0 graphics.py
badd +24 ~/Enseignement/L1/Info5/Pacman/pacman.py
badd +0 phantoms.py
if exists('s:wipebuf') && len(win_findbuf(s:wipebuf)) == 0
  silent exe 'bwipe ' . s:wipebuf
endif
unlet! s:wipebuf
set winheight=1 winwidth=20 shortmess=filnxtToOS
let &winminheight = s:save_winminheight
let &winminwidth = s:save_winminwidth
let s:sx = expand("<sfile>:p:r")."x.vim"
if filereadable(s:sx)
  exe "source " . fnameescape(s:sx)
endif
let &g:so = s:so_save | let &g:siso = s:siso_save
nohlsearch
doautoall SessionLoadPost
unlet SessionLoad
" vim: set ft=vim :
