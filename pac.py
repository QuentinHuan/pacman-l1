from phantoms import random_direction

pac_pos = [1,1]
pac_dir = [0,1]

# move the player one cell in a specified direction
# if the cell is a wall, do nothing
def pac_move(T, direction):
    new_x = ( pac_pos[0] + direction[0] ) % len(T)
    new_y = ( pac_pos[1] + direction[1] ) % len(T[0])
    if(T[new_y][new_x] != 1):
        pac_pos[0] = new_x
        pac_pos[1] = new_y

# move the player using a random direction until it bump into a wall (like phantoms)
# usefull if we do not have keyboard support
def pac_auto_move(T):
    p = pac_pos
    d = pac_dir
    new_p = p.copy()
    new_p[0] = (p[0] + d[0])%len(T)
    new_p[1] = (p[1] + d[1])%len(T[0])
    if (T[new_p[1]][new_p[0]] != 1):
        pac_pos[0] = new_p[0]
        pac_pos[1] = new_p[1]
    else:
        direction = random_direction(d)
        pac_dir[0] = direction[0]
        pac_dir[1] = direction[1]
