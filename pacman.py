from graphics import *
from time import *
from level import *
from phantoms import *
from pac import *

# keyboard support
from getkey import getkey, keys

#######################################################################
#                           PATTERN DRAWING                           #
#######################################################################

def test_pattern_draw(b_animate=False):
    hide_cursor()
    clear()

#      T = pattern_1(20) # test graphics code by drawing a colorfull pattern
    T = pattern_2(20) # another pattern

    # draw level
    draw(T, game_grid_origin_x, game_grid_origin_y)
    show_cursor()

def test_pattern_animate():
    hide_cursor()
    clear()

#      T = pattern_1(20) # test graphics code by drawing a colorfull pattern
    T = pattern_2(20) # another pattern

    t1 = time()
    while(1):
        if (abs(t1-time()) > 0.1): # every 0.1 seconds, offset the pattern and draw it to the screen
            T = pattern_offset(T)
            draw(T, game_grid_origin_x, game_grid_origin_y)
            print()
            t1 = time()

    # draw level
    draw(T, game_grid_origin_x, game_grid_origin_y)
    show_cursor()

#######################################################################
#                                MAIN                                 #
#######################################################################


def main():
    hide_cursor()
    clear()

    # load level
    T = read_file("level1.lvl")

    # last key pressed by player. Reset to {None} when the player move
    last_key = None

    # save the time of the last game update
    update_timer = time()
    while(1):

        # keyboard support
        #  key = getkey(blocking=False)
        # change last_key only if a movement key is pressed
        #  if(key in [keys.W, keys.S, keys.D, keys.A]):
        #      last_key = key

        # every 0.1 seconds, move the phantoms and draw the game
        if (abs(update_timer-time()) > 0.1):
            ## draw functions
            # draw the level
            draw(T)
            # draw the phantoms
            draw_phantoms(phantoms_p, phantoms_color)
            # draw the player
            draw_pacman(pac_pos)
            # force buffer flush
            print()

            ## logic functions

            # update phantoms position
            phantoms_move(T)

            ## move pacman

            # keyboard support
            #  if(last_key == keys.W):
            #      pac_move(T, [0,-1])
            #      last_key = None
            #  if(last_key == keys.S):
            #      pac_move(T, [0,1])
            #      last_key = None
            #  if(last_key == keys.D):
            #      pac_move(T, [1,0])
            #      last_key = None
            #  if(last_key == keys.A):
            #      pac_move(T, [-1,0])
            #      last_key = None

            # if no keyboard, automatic move (like phantoms)
            pac_auto_move(T)

            # reset the timer
            update_timer = time()


    show_cursor()

main() # launch the game

# pattern drawing and animation example
#  test_pattern_draw()
#  test_pattern_animate()
