from constant import *

def hide_cursor():
    print("\x1b[?25l") # hidden

def show_cursor():
    print("\x1b[?25h") # shown

def clear():
    print("\033[2J")

def move_cursor(x, y):
    print("\033[%d;%dH" % (y, x),end='')

# draw a character <marker> (e.g. "\u2588" for a plain square) on terminal screen at position (x,y)
def add_marker(marker, position_x, position_y):
        move_cursor(position_x, position_y)
        print(marker, end='')

# draw static elements (walls, spheres, super_spheres)
def draw(T):
    for i in range(len(T)):
        for j in range(len(T[0])):
            marker=""            
            if(T[i][j] == 0):
                marker = " "
            if(T[i][j] == 1):
                marker =bcolors.GRAY+"\u2588"+bcolors.ENDC
            if(T[i][j] == 2):
                marker =bcolors.YELLOW+"\u2588"+bcolors.ENDC
            if(T[i][j] == 3):
                marker =bcolors.GREEN+"\u2588"+bcolors.ENDC
            if(T[i][j] == 4):
                marker =bcolors.MAGENTA+"\u2588"+bcolors.ENDC
            if(T[i][j] == 5):
                marker =bcolors.YELLOW+"\u2022"+bcolors.ENDC
            if(T[i][j] == 6):
                marker =bcolors.YELLOW+"\u25C9"+bcolors.ENDC

            add_marker(marker, game_grid_origin_x + j, game_grid_origin_y + i)


def draw_phantoms(phantoms_p, phantoms_color):
    for i in range(len(phantoms_p)):
        p = phantoms_p[i]
        marker =phantoms_color[i]+"\u2689"+bcolors.ENDC
        add_marker(marker, game_grid_origin_x + p[0], game_grid_origin_y + p[1])


def draw_pacman(pac_pos):
    marker = bcolors.YELLOW
    marker = marker+"\u25CF"+bcolors.ENDC
    add_marker(marker, game_grid_origin_x + pac_pos[0], game_grid_origin_y + pac_pos[1])

















