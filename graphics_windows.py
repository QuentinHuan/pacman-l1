from constant import *
from colorama import just_fix_windows_console, Cursor
import os
just_fix_windows_console()

MINY, MAXY = 1, 24
MINX, MAXX = 1, 80


def clear_screen():
    os.system('cls')

def hide_cursor():
    print("\x1b[?25l") # hidden

def show_cursor():
    print("\x1b[?25h") # shown

def move_cursor(x, y):
    pos = lambda y, x: Cursor.POS(x, y)
    print(pos(y, x), end='')
    
def add_marker(marker, position_x, position_y):
        move_cursor(position_x, position_y)
        print(marker, end='')

def draw():
    for i in range(0,10):
        for j in range(0,10):
            add_marker("\u2588", i + 5, j + 5)
    print("")
    
