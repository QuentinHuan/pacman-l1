from random import *

# positions of all the phantoms
phantoms_p = []
# directions of all the phantoms
phantoms_d = []
# colors of all the phantoms
phantoms_color = []
# phantoms counter
phantoms_number = 0

# choose a direction at random, different from prev_dir
def random_direction(prev_dir):
    directions = [[0,1], [0,-1], [1,0], [-1,0]]

    random_id = randrange(0, len(directions))
    direction = directions[random_id]
    while(direction == prev_dir):
        random_id = randrange(0, len(directions))
        direction = directions[random_id]
    return direction

# add a phantom at position with a specific color
def init_phantom(position, color):
    global phantoms_number
    phantoms_p.append(position)
    phantoms_color.append(color)
    phantoms_d.append(random_direction([0,0]))
    phantoms_number = phantoms_number + 1

# move all phantoms at random 
def phantoms_move(T):
    for i in range(phantoms_number):
        p = phantoms_p[i]
        d = phantoms_d[i]
        new_p = p.copy()
        new_p[0] = (p[0] + d[0])%len(T)
        new_p[1] = (p[1] + d[1])%len(T[0])
        if (T[new_p[1]][new_p[0]] != 1):
            phantoms_p[i] = new_p
        else:
            direction = random_direction(d)
            phantoms_d[i] = direction

