from random import *
from constant import *
from phantoms import *
# read the file describing the level
def read_file(path):
    T = []
    i=0
    with open(path, encoding="utf-8") as f:
        for line in f:
            L=[]
            j=0
            for char in line:
                if char == ' ':
                    L.append(0)
                if char == '#':
                    L.append(1)
                if char == 'x':
                    L.append(2)
                if char == 'h':
                    L.append(3)
                if char == 'b':
                    L.append(4)
                if char == '.':
                    L.append(5)
                if char == 'o':
                    L.append(6)

                if char == 'g':
                    random_id = randrange(0, len(PHANTOMS_COLORS))
                    init_phantom([i,j], PHANTOMS_COLORS[random_id])
                    L.append(0)
                j=j+1
            T.append(L)
            i=i+1
    return T

# return an array of the form:
# 
#[[0,1,2,3,0,1,2],
# [0,1,2,3,0,1,2],
# [0,1,2,3,0,1,2],
# [0,1,2,3,0,1,2],
# [0,1,2,3,0,1,2],
# [0,1,2,3,0,1,2],
# [0,1,2,3,0,1,2]]
def pattern_1(n):
    T=[]
    for i in range(n):
        L=[]
        for j in range(n):
            L.append(j%4)
        T.append(L)
    return T

    # list comprehension version : a lot shorter !
    #  return [ [j%4 for j in range(n)] for i in range(n)]

# return an array of the form:
# 
#[[0,1,2,3,0,1,2],
# [1,2,3,0,1,2,3],
# [2,3,0,1,2,3,0],
# [3,0,1,2,3,0,1],
# [0,1,2,3,0,1,2],
# [1,2,3,0,1,2,3],
# [2,3,0,1,2,3,0]]
def pattern_2(n):
    T=[]
    for i in range(n):
        L=[]
        for j in range(n):
            L.append((i+j)%4)
        T.append(L)
    return T

    # list comprehension version : a lot shorter !
    return [ [(i+j)%4 for j in range(n)] for i in range(n)]


# offset the pattern by 1 to the left
def pattern_offset(T):
    n = len(T)
    for i in range(n):
        for j in range(n):
            T[i][j] = (T[i][j] + 1)%4 # since the pattern is periodic, we can offset it by adding one and using the modulo operator to wrap the overflowing values back to 0
    return T

